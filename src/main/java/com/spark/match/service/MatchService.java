package com.spark.match.service;

import java.util.List;
import java.util.Optional;

import com.spark.match.exception.EntityNotFoundException;
import com.spark.match.model.FilterCriteria;
import com.spark.match.model.Match;
import com.spark.match.model.MatchContact;

public interface MatchService {	
	List<Match> initializeMatchData();
	List<Match> getMatches(FilterCriteria filterCriteria);
	Optional<Match> getMatchById(Long id);
	Match addContact(MatchContact matchContact) throws EntityNotFoundException;
	Match updatematch(Long id, Match match) throws Exception;
	Match removeFromContact(MatchContact matchContact) throws EntityNotFoundException;
}
