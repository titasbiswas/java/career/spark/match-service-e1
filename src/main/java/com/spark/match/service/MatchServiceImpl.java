package com.spark.match.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spark.match.exception.EntityNotFoundException;
import com.spark.match.model.Contact;
import com.spark.match.model.FilterCriteria;
import com.spark.match.model.Match;
import com.spark.match.model.MatchContact;
import com.spark.match.repo.ContactRepository;
import com.spark.match.repo.MatchRepository;
import com.spark.match.util.JsonDbUtils;

@Service
public class MatchServiceImpl implements MatchService {

	private final String JSON_SAMPLE_DATA_FILE_PATH = "/json/matches.json";

	@Autowired
	// JsonDbUtils<User, JpaRepository<User,Long>, Long> jsonDbUtils;
	private JsonDbUtils jsonDbUtils;

	@Autowired
	private MatchRepository matchRepository;
	
	@Autowired
	private ContactRepository contactRepository;

	public List<Match> initializeMatchData() {
		return jsonDbUtils.insertJsonToMatchTable(JSON_SAMPLE_DATA_FILE_PATH);
	}

	@Override
	public List<Match> getMatches(FilterCriteria filterCriteria) {
		
		Match currentMatch = matchRepository.findById(filterCriteria.getCurrentId()).get();
		List<Match> matches = matchRepository.getMatches(filterCriteria);
		if (filterCriteria.isInContact()) {
			return matches.stream().filter(m -> {
				return currentMatch.getMates().contains(m) || currentMatch.getContacts().contains(m);
			}).collect(Collectors.toList());
		}
		else
			return matches;
	}

	@Override
	public Optional<Match> getMatchById(Long id) {
		return matchRepository.findById(id);
	}

	@Override
	@Transactional
	public Match addContact(MatchContact matchContact) throws EntityNotFoundException {
		/*Match matchToBeAdded = matchRepository.findById(contact.getInContactWith()).map(contactMatch -> {
			return contactMatch;
		}).orElseThrow(() -> new EntityNotFoundException(Match.class, "id", contact.getInContactWith().toString()));*/

		//Match contact = matchRepository.findById(matchContact.getContact().getId()).get();
		return matchRepository.findById(matchContact.getMatch().getId()).map(match -> {
			if ((!match.getContacts().contains(matchContact.getContact()))
					&& (!match.getMates().contains(matchContact.getContact()))
					) {
				/*match.getContacts()
						.add(Contact.builder()
								.matchId(match.getId())
								.inContactWith(matchToBeAdded.getId()).build());*/
				match.addContact(matchContact.getContact());
				matchRepository.save(match);
			}
			return matchContact.getContact();
		}).orElseThrow(() -> new EntityNotFoundException(Match.class, "id", matchContact.getContact().getId().toString()));
	}
	
	@Override
	public Match removeFromContact(MatchContact matchContact) throws EntityNotFoundException {
		/*return contactRepository.findByMatchIdAndInContactWith(contact.getMatchId(), contact.getInContactWith()).map(c->{
			contactRepository.delete(c);
			return matchRepository.findById(c.getInContactWith()).get();
		}).orElseThrow(() -> new EntityNotFoundException(Match.class, "id", contact.getId().toString()));*/
		
		return matchRepository.findById(matchContact.getMatch().getId()).map(match -> {
			if (match.getContacts().contains(matchContact.getContact())) {
				match
						/*.add(Contact.builder()
								//.matchId(match.getId())
								.inContactWith(matchToBeAdded.getId()).build());*/
				.removeContact(matchContact.getContact());
				matchRepository.save(match);
			}
			return matchContact.getContact();
		}).orElseThrow(() -> new EntityNotFoundException(Match.class, "id", matchContact.getContact().getId().toString()));
	
		
	}

	@Override
	public Match updatematch(Long id, Match match) throws Exception {
		return matchRepository.findById(id).map(m -> {
			m.setAge(match.getAge());
			m.setDisplayName(match.getDisplayName());
			m.setHeightInCm(match.getHeightInCm());
			m.setJobTitle(match.getJobTitle());
			m.setReligion(match.getReligion());
			return matchRepository.save(m);
		}).orElseThrow(() -> new EntityNotFoundException(Match.class, "id", id.toString()));

	}
}
