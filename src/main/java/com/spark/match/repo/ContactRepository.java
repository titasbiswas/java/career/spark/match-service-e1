package com.spark.match.repo;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.spark.match.model.Contact;

public interface ContactRepository extends JpaRepository<Contact, Long>{

	Optional<Contact> findByMatchIdAndInContactWith(Long matchId, Long inContactWith);
}
