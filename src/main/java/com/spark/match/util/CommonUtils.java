package com.spark.match.util;

import java.math.BigDecimal;

public class CommonUtils {

	public static BigDecimal convertPrcntgToFrctn(int prcntg) {
		BigDecimal p = new BigDecimal(prcntg);
		return p.divide(new BigDecimal("100"));
	}
}
