package com.spark.match.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.spark.match.model.Contact;
import com.spark.match.model.Match;
import com.spark.match.model.json.MatchJson;
import com.spark.match.repo.MatchRepository;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
//public class JsonDbUtils<T, R extends JpaRepository<E, L>, L, E> {
public class JsonDbUtils {

	/*@Autowired
	private R repo;
	
	public  List<T> insertJsonToDb(String jsonFilePath) {
		List<T> savedObjects = new ArrayList<T>();
		ObjectMapper mapper = new ObjectMapper();
		TypeReference<List<T>> typeReference = new TypeReference<List<T>>(){};
		InputStream inputStream = TypeReference.class.getResourceAsStream(jsonFilePath);
		try {
			List<T> objects = mapper.readValue(inputStream,typeReference);
			
			objects.forEach(obj ->{
				savedObjects.add(repo.save(obj));
			});
			System.out.println("objects saved");
			
		} catch (IOException e){
			System.out.println("Unable to save users: " + e.getMessage());
		}
		return savedObjects;
	}*/
	
	@Autowired
	private MatchRepository matchRepository;
	
	public List<Match> insertJsonToMatchTable(String jsonFilePath){
		List<Match> insertedMatches = null;
		ObjectMapper mapper = new ObjectMapper();
		TypeReference<List<MatchJson>> typeReference = new TypeReference<List<MatchJson>>(){};
		InputStream inputStream = TypeReference.class.getResourceAsStream(jsonFilePath);
		try {
			List<MatchJson> jsonMatches = mapper.readValue(inputStream,typeReference);
			insertedMatches = 
			jsonMatches.parallelStream().map(jsonMatch ->{
				Match tempMatch = new Match();
				tempMatch.setAge(jsonMatch.getAge());
				tempMatch.getCity().setName(jsonMatch.getCity().getName());
				tempMatch.getCity().setLat(jsonMatch.getCity().getLat());
				tempMatch.getCity().setLon(jsonMatch.getCity().getLon());
				tempMatch.setCompatibilityScore(jsonMatch.getCompatibility_score());
				tempMatch.setContactsExchanged(jsonMatch.getContacts_exchanged());
				tempMatch.setDisplayName(jsonMatch.getDisplay_name());
				tempMatch.setFavourite(jsonMatch.getFavourite());
				tempMatch.setHeightInCm(jsonMatch.getHeight_in_cm());
				tempMatch.setJobTitle(jsonMatch.getJob_title());
				tempMatch.setMainPhoto(jsonMatch.getMain_photo());
				tempMatch.setReligion(jsonMatch.getReligion());
				jsonMatch.getContacts().forEach(c -> {
					/*Contact contact = new Contact();
					contact.setInContactWith(c);
					tempMatch.getContacts().add(contact);*/
				});
				return matchRepository.save(tempMatch);
			}).collect(Collectors.toList());
			
			System.out.println("Users Saved!");
		} catch (IOException e){
			System.out.println("Unable to save users: " + e.getMessage());
		}
		
		return insertedMatches;
	}
}
