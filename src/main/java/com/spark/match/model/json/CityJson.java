package com.spark.match.model.json;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CityJson {
	

	private String name;
	private BigDecimal lat;
	private BigDecimal lon;

}
