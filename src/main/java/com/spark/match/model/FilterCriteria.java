package com.spark.match.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class FilterCriteria {
	
	private Long currentId;
	private boolean hasPhoto;
	private boolean inContact;
	private boolean favourite;
	private Range compatibilityScore;
	private Range age;
	private Range height;
	private LocationDistance locDist;

}
