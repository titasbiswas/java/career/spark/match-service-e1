package com.spark.match.model;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "matches")
public class Match {
	@Id
	@SequenceGenerator(name = "match_seq_generator", sequenceName = "matches_id_seq", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "match_seq_generator")
	private Long id;

	@Column(name = "display_name")
	private String displayName;
	private Integer age;
	@Column(name = "job_title")
	private String jobTitle;
	@Column(name = "height_in_cm")
	private Integer heightInCm;
	@Embedded
	private City city = new City();
	@Column(name = "main_photo")
	private String mainPhoto;
	@Column(name = "compatibility_score")
	private BigDecimal compatibilityScore;
	@Column(name = "contacts_exchanged")
	private Integer contactsExchanged;
	private Boolean favourite;
	private String religion;

	/*@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
	@JoinColumn(name = "match_id")
	@Builder.Default
	private List<Contact> contacts = new ArrayList<Contact>();
*/
	
	@ManyToMany(cascade={CascadeType.ALL}, fetch=FetchType.LAZY)
	@JoinTable(name="match_contact",
		joinColumns={@JoinColumn(name="match_id")},
		inverseJoinColumns={@JoinColumn(name="contact_id")})
	@Builder.Default
	private Set<Match> contacts = new HashSet<Match>();
	
	@ManyToMany(mappedBy="contacts")
	@Builder.Default
	@JsonIgnore
	private Set<Match> mates = new HashSet<Match>();
	
	public void addContact(Match contact) {
		contacts.add(contact);
		contact.getMates().add(this);
	}
	
	public void removeContact(Match contact) {
		contacts.remove(contact);
		contact.getMates().remove(this);
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null || obj.getClass() != this.getClass())
			return false;
		Match matchObj = (Match) obj;
		return matchObj.id.equals(this.id);
	}

	@Override
	public int hashCode() {
		return this.id.intValue();
	}

}
