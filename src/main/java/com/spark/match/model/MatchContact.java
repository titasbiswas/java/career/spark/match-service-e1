package com.spark.match.model;

import java.io.Serializable;

import lombok.Data;

@Data
public class MatchContact implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Match match;
	private Match contact;

}
