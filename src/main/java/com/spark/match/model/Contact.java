package com.spark.match.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.spark.match.model.Match.MatchBuilder;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name="contacts")
public class Contact {
	
	@Id
	@SequenceGenerator(name="contact_seq_generator", sequenceName = "contacts_id_seq", allocationSize=1, initialValue=1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "contact_seq_generator")	
	private Long id;

	@Column(name="match_id")
	private Long matchId;
	
	@Column(name="in_contact_with")
	private Long inContactWith;
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null || obj.getClass() != this.getClass())
			return false;
		Contact contactObj = (Contact) obj;
		return (contactObj.matchId.equals(this.matchId) && contactObj.inContactWith.equals(this.inContactWith));
	}

	@Override
	public int hashCode() {
		return this.matchId.intValue()+this.inContactWith.intValue();
	}
}
