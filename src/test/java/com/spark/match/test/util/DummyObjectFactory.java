package com.spark.match.test.util;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;

public class DummyObjectFactory {
	
	public static Object populateDummyObject(Object obj) throws IllegalArgumentException, IllegalAccessException {
        
        Field[] fields = obj.getClass().getDeclaredFields();

        for(Field field:fields){
            if(!field.isAccessible()){
            	field.setAccessible(true);
                Class<?> type = field.getType();

                if(type.equals(Integer.class)){
                	field.set(obj, 100); //Set Default value
                }else if (type.equals(Integer.class)){
                	field.set(obj, Integer.parseInt("10"));
                }else if(type.equals(String.class)){
                	field.set(obj, "Default");
                }else if (type.equals(Long.class)){
                	field.set(obj, Long.parseLong("310"));
                }else if (type.equals(BigDecimal.class)){
                	field.set(obj, new BigDecimal(303));
                }else if (type.equals(Timestamp.class)){
                	field.set(obj, Timestamp.valueOf("2007-09-23 10:10:10.0"));
                }else if (type.equals(Date.class)){
                	field.set(obj, new Date());
                }else if (type.equals(java.sql.Date.class)){
                	field.set(obj, new java.sql.Date(System.currentTimeMillis()));
                }
                field.setAccessible(false);
            }
            //System.out.println(f.get(se)); //print fields with reflection
        }
		return obj;
    }

}
