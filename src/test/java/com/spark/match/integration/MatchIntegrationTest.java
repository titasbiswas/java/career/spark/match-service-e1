package com.spark.match.integration;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.spark.match.model.City;
import com.spark.match.model.Match;
import com.spark.match.repo.MatchRepository;
import com.spark.match.util.JsonDbUtils;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application-test.properties")
public class MatchIntegrationTest {

	@Autowired
	private MockMvc mvc;

	@Autowired
	private MatchRepository matchRepository;

	@Autowired
	private JsonDbUtils jsonDbUtils;

	private List<Match> insertedTestData;
	private Match referenceData;

	@Before
	public void setupTestPrerequisites() {
		insertedTestData = jsonDbUtils.insertJsonToMatchTable("/json/testData.json");
	}

	@Test
	public void testGetMatchById() throws Exception {
		referenceData = insertedTestData.get(0);

		mvc.perform(get("/match/{id}", referenceData.getId()))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.displayName", is(referenceData.getDisplayName())));
	}

	@Test
	public void testGetFilteredMatches() throws Exception {

		mvc.perform(post("/match/filter").accept(MediaType.APPLICATION_JSON_VALUE)
				.contentType(MediaType.APPLICATION_JSON_VALUE).content("{}"))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$", hasSize(4)));
	}

	@Test
	public void testGetFilteredMatches_shouldReturnMatchesByDistance() throws Exception {

		mvc.perform(post("/match/filter").accept(MediaType.APPLICATION_JSON_VALUE)
				.contentType(MediaType.APPLICATION_JSON_VALUE)
				.content("{\"locDist\":{\"lat\":51.568535, \"lon\":-1.772232, \"distanceInKm\":100}}"))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$", hasSize(2)));
	}
	
	@Test
	public void testGetFilteredMatches_shouldThrowBadRequest() throws Exception {

		mvc.perform(post("/match/filter").accept(MediaType.APPLICATION_JSON_VALUE)
				.contentType(MediaType.APPLICATION_JSON_VALUE)
				.content("fjfjfj"))
				.andExpect(status().isBadRequest());
	}

	@After
	public void cleanUp() {
		matchRepository.deleteAll(insertedTestData);

	}

}
